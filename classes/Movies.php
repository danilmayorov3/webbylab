<?php
namespace Classes;

class Movies extends Db{
    public function getMovies()
    {
        $pdo = $this->connect();
        if (isset($_POST['search_btn'])) {
            $select = 'SELECT * FROM movies WHERE is_deleted = 0 AND (title LIKE "%' . $_POST['search'] . '%" or stars like "%' . $_POST['search'] . '%" )';
            $sql = $pdo->prepare("$select");
            $sql->execute();
            $result = $sql->fetchAll();
        } else {
            $sql = $pdo->prepare("SELECT * FROM movies WHERE is_deleted = 0");
            $sql->execute();
            $result = $sql->fetchAll();
        }
        return $result;
    }

    public function createMoviesFromFile($file)
    {
        $arr = [];
        $count = 0;
        $exeptions = 0;

        try {
            while (!feof($file)) {
                $str = fgets($file);
                $cut_str = strstr($str, ':');
                if (strlen($cut_str) > 3){
                    $cut_str = str_replace(':', '', $cut_str);
                    $arr[] = $cut_str;
                    $count++;
                }
                if ($count  > 3){
                    $sql = ("INSERT movies (title, release_year, format, stars) values (?,?,?,?)");
                    $pdo = $this->connect();
                    $query = $pdo->prepare($sql);
                    if (!$query->execute([trim($arr[0]), trim($arr[1]), trim($arr[2]), trim($arr[3])])){
                        $exeptions++;
                    }
                    $count = 0;
                    $arr = [];
                }
            }
        } catch (PDOException $e){
            echo 'Connection error: ' . $e->getMessage();
        }
        if ($exeptions > 0)
        {
            $_SESSION['errors'][] = [
                'title' => 'create_from_file',
                'text' => 'Не удалось добавить фильм(ы), фильм(ы) с таким названием уже существует'
            ];
        } else {
            $_SESSION['success_file'] = 'Фильмы загружены из файла';
        }

        return true;
    }

    public function deleteMovies($id)
    {
        $sql = ("UPDATE movies SET is_deleted=? WHERE id=?");
        $pdo = $this->connect();
        $query = $pdo->prepare($sql);

        if ($query->execute([true, $id])){
            $_SESSION['success_delete'] = 'Фильм удален';
        }

    }

    public function createMovie($title, $release_year, $format, $stars)
    {
        $sql = ("INSERT movies (title, release_year, format, stars) values (?,?,?,?)");
        $pdo = $this->connect();
        $query = $pdo->prepare($sql);
        if ($query->execute([$title, $release_year, $format, $stars])){
            $_SESSION['success_create'] = 'Фильм добавлен';
        } else {
            $_SESSION['errors'][] = [
                'title' => 'create',
                'text' => 'Не удалось добавить фильм, фильм с таким названием уже существует'
            ];
        }
    }

    public function updateMovie($title, $release_year, $format, $stars, $id)
    {
        $sql = ("UPDATE movies SET title=?, release_year=?, format=?, stars=? WHERE id=?");
        $pdo = $this->connect();
        $query = $pdo->prepare($sql);

        if ($query->execute([$title, $release_year, $format, $stars, $id])){
            $_SESSION['success_edit'] = 'Фильм изменен';
        }
    }
}