<?php

namespace Classes;

class Validator {

    private $data;
    private static $fields = ['title', 'release_year', 'format', 'stars'];

    public function __construct($post_data){
        $this->data = $post_data;
    }

    public function validateForm(){
        foreach(self::$fields as $field){
            if(!array_key_exists($field, $this->data)){
                trigger_error("'$field' is not present in the data");
                return;
            }
        }
        $_SESSION['errors'] = [];
        $this->validateTitle();
        $this->validateRelease();
        $this->validateStars();
        $this->validateFormat();

        if (!empty($_SESSION['errors'])){
            return false;
        } else {
            return true;
        }

    }

    private function validateTitle(){

        $val = trim($this->data['title']);

        if(empty($val)){
            return $this->addError('title', 'Название не может быть пустым');
        } else {
            if(!preg_match('/^[a-zA-Zа-яёА-ЯЁ0-9 ]{3,50}$/u', $val)){
                return $this->addError('title','Название должно быть от 3 до 50 символов, можно использовать только буквы и цифры');
            }
        }

    }

    private function validateRelease(){

        $val = trim($this->data['release_year']);
        if(empty($val)){
            return $this->addError('release_year', 'Год выпуска не может быть пустым');
        } else {
            if(!preg_match('/^[0-9]{4}$/u', $val) || !($val >= 1850 && $val <= 2021)){
                return $this->addError('release_year', 'Год должен быть в промежутке между 1850 и 2021');
            }
        }

    }

    private function validateStars(){

        $val = trim($this->data['stars']);

        if(empty($val)){
            return $this->addError('stars', 'Список актеров не должен быть пустым');
        } else {
            if(!preg_match('/^[a-zA-Zа-яёА-ЯЁ, -]{1,300}$/u', $val)){
                return $this->addError('stars','Список актеров должен быть до 300 символов');
            }
        }

    }

    private function validateFormat(){

        $val = trim($this->data['format']);

        if(empty($val)){
            return $this->addError('format', 'Формат не может быть пустым');
        } else {
            if(!preg_match('/^[a-zA-Zа-яёА-ЯЁ -]{1,10}$/u', $val)){
                return $this->addError('format','format must be 1-225 chars & alphanumeric');
            }
        }

    }

    private function addError($key, $val){
        $_SESSION['errors'][] = [
            'title' => $key,
            'text' => $val
        ];
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        return false;
    }

}