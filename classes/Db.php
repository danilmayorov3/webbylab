<?php
namespace Classes;
use PDO;
use PDOException;

class Db
{
private $host = 'localhost';
private $db = 'webbylab';
private $user = 'root';
private $password = '';

    public function connect()
    {
        try {
            $pdo = new PDO("mysql:host=" .$this->host .";dbname=" . $this->db, $this->user, $this->password);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            return $pdo;
        } catch (PDOException $e){
            echo 'Connection error: ' . $e->getMessage();
        }
    }
}
