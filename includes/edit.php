<div class="modal fade" id="edit<?= $res['id'] ?>" tabindex="-1" aria-labelledby="edit<?= $res['id'] ?>" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/formaction/action.php?id=<?= $res['id'] ?>" method="post">
                    <div class="form-group">
                        <div class="mb-3">
                            <label for="title" class="form-label">Название</label>
                            <input type="text" class="form-control" id="title" name="title" value="<?= $res['title'] ?>">
                        </div>
                        <div class="mb-3">
                            <label for="release_year" class="form-label">Год выпуска</label>
                            <input type="text" class="form-control" id="release_year" name="release_year" value="<?= $res['release_year'] ?>">
                        </div>
                        <div class="mb-3">
                            <label for="format" class="form-label">Формат</label>
                            <input type="text" class="form-control" id="format" name="format" value="<?= $res['format'] ?>">
                        </div>
                        <div class="mb-3">
                            <label for="stars" class="form-label">Список актеров</label>
                            <input type="text" class="form-control" id="stars" name="stars" value="<?= $res['stars'] ?>">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary" name="edit">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>