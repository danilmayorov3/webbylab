<div class="modal fade" id="delete<?= $res['id'] ?>" tabindex="-1" aria-labelledby="delete<?= $res['id'] ?>" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вы уверены что хотите удалить этот фильм? <?= $res['title'] ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-footer">
                <form action="/formaction/action.php?id=<?= $res['id'] ?>" method="post">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-danger" name="delete">Удалить</button>
                </form>
            </div>
        </div>
    </div>
</div>