<?php
use Classes\Movies;
include 'includes/alert.php';
?>
<div class="container">
    <form action="/" method="post">
        <div class="row mt-3">
            <div class="col-md-4">
                <input type="text" class="form-control" id="search" name="search">
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary" name="search_btn">Поиск</button>
            </div>
            <div class="col-md-1">
                <button type="button"
                        data-bs-toggle="modal"
                        data-bs-target="#create"
                        class="btn btn-success">

                    <i class="fas fa-plus"></i>
                </button>
            </div>
    </form>
    <?php include 'includes/create.php' ?>
    <div class="col-md-1">
        <button type="button"
                data-bs-toggle="modal"
                data-bs-target="#file_upload"
                class="btn btn-success">

            <i class="fas fa-folder"></i>
        </button>
    </div>
</div>
<?php include 'includes/upload_from_file.php' ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-hover mt-3">
            <thead class="table-dark">
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Год выпуска</th>
                <th>Формат</th>
                <th>Список актеров</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php $movies = new Movies(); ?>
            <?php foreach ($movies->getMovies() as $res): ?>
                <?php include 'includes/edit.php'; ?>
                <?php include 'includes/delete.php'; ?>
                <tr>
                    <td><?= $res['id']; ?></td>
                    <td><?= $res['title']; ?></td>
                    <td><?= $res['release_year']; ?></td>
                    <td><?= $res['format']; ?></td>
                    <td><?= $res['stars']; ?></td>
                    <td>
                        <form action="/formaction/action.php?id=<?= $res['id']; ?>" method="post">
                            <a href="?id=<?= $res['id']; ?>"
                               data-bs-toggle="modal"
                               data-bs-target="#edit<?= $res['id']; ?>"
                               class="btn btn-success">

                                <i class="fas fa-pen"></i>
                            </a>
                            <a href="?id=<?= $res['id']; ?>"
                               data-bs-toggle="modal"
                               data-bs-target="#delete<?= $res['id']; ?>"
                               class="btn btn-danger">

                                <i class="fas fa-trash"></i>
                            </a>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
