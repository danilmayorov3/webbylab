<?php
session_start();
if(isset($_SESSION['errors']))
{
    foreach ($_SESSION['errors'] as $error)
    {
        ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?= $error['text']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <?php
    }
    unset($_SESSION['errors']);
}
if(isset($_SESSION['title']))
{
    ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $_SESSION['title']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['title']);
}
if(isset($_SESSION['release_year']))
{
    ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $_SESSION['release_year']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['release_year']);
}
if(isset($_SESSION['format']))
{
    ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $_SESSION['format']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['format']);
}
if(isset($_SESSION['stars']))
{
    ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $_SESSION['stars']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['stars']);
}
if(isset($_SESSION['file']))
{
    ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $_SESSION['file']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['file']);
}
if(isset($_SESSION['success_create']))
{
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?= $_SESSION['success_create']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['success_create']);
}
if(isset($_SESSION['success_edit']))
{
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?= $_SESSION['success_edit']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['success_edit']);
}
if(isset($_SESSION['success_delete']))
{
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?= $_SESSION['success_delete']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['success_delete']);
}
if(isset($_SESSION['success_file']))
{
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?= $_SESSION['success_file']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
    unset($_SESSION['success_file']);
}
?>