<?php
include '../vendor/autoload.php';

use Classes\Movies;
use Classes\Validator;

session_start();
$movies = new Movies();

if (isset($_POST['create'])) {
    $title = htmlspecialchars($_POST['title']);
    $release_year = htmlspecialchars($_POST['release_year']);
    $format = htmlspecialchars($_POST['format']);
    $stars = htmlspecialchars($_POST['stars']);
    $validation = new Validator($_POST);
    if ($validation->validateForm()){
        $movies->createMovie(trim($title), trim($release_year), trim($format), trim($stars));
    }
    header('Location: ' . $_SERVER['HTTP_REFERER']);

} else if (isset($_POST['delete'])) {
    $id = $_GET['id'];
    $movies->deleteMovies($id);
    header('Location: ' . $_SERVER['HTTP_REFERER']);

} else if (isset($_POST['edit'])) {
    $title = htmlspecialchars($_POST['title']);
    $release_year = htmlspecialchars($_POST['release_year']);
    $format = htmlspecialchars($_POST['format']);
    $stars = htmlspecialchars($_POST['stars']);
    $id = $_GET['id'];
    $validation = new Validator($_POST);
    if ($validation->validateForm()){
        $movies->updateMovie($title, $release_year, $format, $stars, $id);
    }
    header('Location: ' . $_SERVER['HTTP_REFERER']);

} else if (isset($_POST['file_upload'])) {
    if ($_FILES['file']['type'] == 'text/plain'){
        $file = fopen($_FILES['file']['tmp_name'], 'r');
        $movies->createMoviesFromFile($file);

        fclose($file);
    } else {
        $_SESSION['file'] = 'Не верный формат файла';
    }

    header('Location: ' . $_SERVER['HTTP_REFERER']);
}
