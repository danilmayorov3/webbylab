DROP DATABASE IF EXISTS webbylab;

CREATE DATABASE webbylab;

USE webbylab;

DROP TABLE IF EXISTS movies;

CREATE TABLE movies(
                       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                       title VARCHAR(255) NOT NULL UNIQUE ,
                       release_year INT NOT NULL,
                       format VARCHAR(255) NOT NULL,
                       stars VARCHAR(255) NOT NULL,
                       is_deleted BOOLEAN NOT NULL DEFAULT 0
);
